# ✓ A player is displayed
# ✓ A player can be moved left and right
# ✓ A ball is displayed
# ✓ A ball has motion
# ✓ A ball bounces off edges
# ✓ A ball can be bounced by the player
# ✓ Bricks can be displayed
# ✓ A brick can be destroyed by a colliding ball
# ✓ A game is lost when the ball passes a line below the player
# ✓ A game can be restarted
# ✓ A game is won when all bricks are destroyed

$dragon.require('app/entity_manager.rb')
$dragon.require('app/main_loop.rb')

def create_game_over_scene
  {
    systems: [
      GameOverInputSystem,
      LabelRenderSystem
    ],
    entities: [
      {
        position: {
          x: 648,
          y: 400
        },
        text: 'Game Over',
        visible: true
      }
    ]
  }
end

def create_win_scene
  {
    systems: [
      GameOverInputSystem,
      LabelRenderSystem
    ],
    entities: [
      {
        position: {
          x: 648,
          y: 400
        },
        text: 'You Win',
        visible: true
      }
    ]
  }
end

def create_initial_scene
  {
    systems: [
      GameLossSystem,
      GameWinSystem,
      InputSystem,
      PlayerMovementSystem,
      BoundarySystem,
      CollisionSystem,
      FlipBallVelocityOnCollisionSystem,
      BreakBrickSystem,
      VelocitySystem,
      RenderSystem
    ],
    entities: [
      *(1..10).map do |count|
        {
          name: 'brick',
          position: {
            x: count * 100,
            y: 500
          },
          size: {
            width: 90,
            height: 20
          },
          collidable: {
            active: true,
            collisions: []
          },
          breakable: {
            is_broken: false
          },
          visible: true
        }
      end,
      {
        name: 'player',
        player_input: {
          moved: false
        },
        position: {
          x: 30,
          y: 30
        },
        size: {
          width: 60,
          height: 10
        },
        visible: true,
        collidable: {
          active: true,
          collisions: []
        }
      },
      {
        name: 'ball',
        position: {
          x: 60,
          y: 300
        },
        size: {
          width: 10,
          height: 10
        },
        velocity: {
          x: 4,
          y: 4,
          simulated: true
        },
        visible: true,
        collidable: {
          active: true,
          collisions: []
        },
        is_out_of_bounds: false
      }
    ]
  }
end

def create_persistent_scene
  {
    systems: [
      SceneChangeSystem
    ],
    entities: [
      {
        game_state: {
          next_scene: nil
        }
      }
    ]
  }
end

module GameOverInputSystem
  extend GameOverInputSystem

  def tick args, entities
    entities.each do |entity|
      next unless args.inputs.mouse.click

      game_state_entity = EntityManager.find_singleton_entity_with_component(args, :game_state)
      game_state_entity.game_state.next_scene = create_initial_scene
    end
  end

  def components
    []
  end
end

module GameWinSystem
  extend GameWinSystem

  def tick args, entities
    return unless entities.count > 0 && entities.all? { |entity| entity.breakable.is_broken }

    game_state_entity = EntityManager.find_singleton_entity_with_component(args, :game_state)
    game_state_entity.game_state.next_scene = create_win_scene
  end

  def components
    [:breakable]
  end
end

module InputSystem
  extend InputSystem

  def tick args, entities
    entities.each do |entity|
      if args.inputs.mouse.moved
        entity.player_input = EntityManager.create_component args, {
          moved: true,
          x: args.inputs.mouse.position.x,
          y: args.inputs.mouse.position.y
        }
      else
        entity.player_input = EntityManager.create_component args, { 
          moved: false 
        }
      end 
    end
  end

  def components
    [:player_input]
  end
end

module RenderSystem
  extend RenderSystem

  def tick args, entities
    entities.each do |entity|
      next unless entity.visible

      args.outputs.solids << [
        entity.position.x,
        entity.position.y,
        entity.size.width,
        entity.size.height,
      ]
    end
  end
  
  def components
    [:position, :size, :visible]
  end
end

module PlayerMovementSystem
  extend PlayerMovementSystem

  def tick args, entities
    entities.each do |entity| 
      next unless entity.player_input.moved

      entity.position.x = entity.player_input.x
    end
  end

  def components
    [:position, :player_input]
  end
end

module VelocitySystem
  extend VelocitySystem

  def tick args, entities
    entities.each do |entity| 
      next unless entity.velocity.simulated
      
      entity.position.x += entity.velocity.x
      entity.position.y += entity.velocity.y
    end
  end

  def components
    [:velocity, :position]
  end
end

module CollisionSystem
  extend CollisionSystem

  def tick args, entities
    entities.each do |entity| 
      entity.collidable.collisions = []

      next unless entity.collidable.active

      entity.collidable.collisions = (entities - [entity]).
        select { |other| other.collidable.active }.
        select do |other|
        ([
          entity.position.x,
          entity.position.y,
          entity.size.width,
          entity.size.height
        ].intersects_rect? [
          other.position.x,
          other.position.y,
          other.size.width,
          other.size.height
        ])
      end
    end
  end

  def components
    [:collidable, :position, :size]
  end 
end

module FlipBallVelocityOnCollisionSystem
  extend FlipBallVelocityOnCollisionSystem

  def tick args, entities
    entities.each do |entity| 
      next unless entity.collidable.collisions.count > 0

      entity.velocity.y = -entity.velocity.y
    end
  end

  def components
    [:velocity, :collidable]
  end
end

module BreakBrickSystem
  extend BreakBrickSystem
  
  def tick args, entities
    entities.each do |entity| 
      next if entity.breakable.is_broken
      next unless entity.collidable.collisions.count > 0

      entity.collidable.active = false
      entity.breakable.is_broken = true
      entity.visible = false
    end
  end

  def components
    [:breakable, :collidable, :visible]
  end
end

module LabelRenderSystem
  extend LabelRenderSystem

  def components 
    [:text, :position, :visible] 
  end

  def tick args, entities
    entities.each do |entity| 
      next unless entity.visible

      args.outputs.labels << [
        entity.position.x,
        entity.position.y,
        entity.text,
        24,
        1
      ]
    end
  end
end

module GameLossSystem
  extend GameLossSystem

  def components
    [:is_out_of_bounds]
  end

  def tick args, entities
    entities.each do |entity|
      next unless entity.is_out_of_bounds

      entity.is_out_of_bounds = false
      game_state_entity = EntityManager.find_singleton_entity_with_component(args, :game_state)
      game_state_entity.game_state.next_scene = create_game_over_scene
    end
  end
end

module SceneChangeSystem
  extend SceneChangeSystem

  def components
    [:game_state]
  end

  def tick args, entities
    entities.each do |entity|
      next_scene = entity.game_state.next_scene
      next unless next_scene

      entity.game_state.next_scene = nil

      EntityManager.replace_scene args, next_scene
    end
  end
end

module BoundarySystem
  extend BoundarySystem

  def tick args, entities
    entities.each do |entity| 
      next unless entity.velocity.simulated
      
      if entity.position.y == 0
        entity.is_out_of_bounds = true
        next
      end

      if (entity.position.x >= args.grid.w) || entity.position.x == 0
        entity.velocity.x = -entity.velocity.x
      end

      if entity.position.y >= args.grid.h
        entity.velocity.y = -entity.velocity.y
      end
    end
  end

  def components
    [:velocity, :position, :is_out_of_bounds]
  end
end
