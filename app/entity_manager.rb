module EntityManager
  extend EntityManager

  def create_gtk_entity args, entity
    gtk_entity = args.game.new_entity :entity, entity

    gtk_entity.hash.each do |component_name, values|
      next unless values.is_a? Hash
      
      gtk_entity[component_name] = args.game.new_entity :component, 
        values
    end

    gtk_entity
  end

  def create_component args, component
    args.game.new_entity :component, component
  end

  def replace_scene args, scene
    args.game.entities = nil

    $systems = [
      *create_persistent_scene[:systems],
      *scene[:systems]
    ]

    args.game.entities = scene[:entities].map do |entity|
      create_gtk_entity args, entity
    end
  end

  def remove_entity args, id
    [
      args.game.persistent_entities,
      args.game.entities
    ].each do |collection|
      collection.delete_if { |entity| entity.entity_id == id}
    end
  end

  def find_all_entities args
    [
      *args.game.persistent_entities,
      *args.game.entities
    ]
  end

  def find_all_entities_with_components args, components
    find_all_entities(args).
      select { |entity| components.all? {|component| entity.hash.keys.include? component } }
  end

  def find_singleton_entity_with_component args, component
    maybe_singleton = find_all_entities_with_component args, component

    maybe_singleton ? maybe_singleton.first : nil
  end

  def find_all_entities_with_component args, component
    find_all_entities_with_components args, 
      [component]
  end

  def tick args
    $systems.each do |mod|
      mod.tick args, 
        find_all_entities_with_components(args, mod.components)
    end
  end
end