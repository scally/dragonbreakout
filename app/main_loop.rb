# HACK: This breaks hot-reload
$setup = false

def tick args
  unless $setup
    args.game.entities = []
    args.game.persistent_entities = create_persistent_scene[:entities].map do |entity|
      EntityManager.create_gtk_entity args, entity
    end

    EntityManager.replace_scene args, create_initial_scene

    $setup = true
  end

  EntityManager.tick args
end